%% Clasification tree using matlab stat libraries

%data source
data = csvread('cmc.data');

%Percentage of the training data specified in percentage, it's only used 
%when the selection of the training set is random
trainSize = .67; 
reg = length(data);

%Total eff over several tries
totals = [0 0 0];

%Use random training set, this uses testSize, otherwise it will use fixed 
%1 - 1000 to training and 1001 - end to validation
useRandomTrainingSet = false;

%Toogle the show of graphs, please toogle them off if you're doing analisys
%over time 
showGraphs = true;

%Number of runs of the algorithm, this affect totals for the evaluation over
%time
nTest=1;

for i = 1:nTest

    if useRandomTrainingSet == true
        k = randperm(reg);

        trnData = data(k(1:round(reg*trainSize)),:);
        valData = data(k(round(reg*trainSize)+1:end),:);
    else
        trnData = data(1:1000,:);
        valData = data(1001:end,:);
    end
    %% Separates predictor from the rest of data and build the tree

    Y = trnData(:,10);
    X = trnData(:,1:9);
    Yv = valData(:,10);
    Xv = valData(:,1:9);
    %X = data(:,2:9);

    %% Deciding the minimum amount of observation per leaf

    leafs = logspace(1,2,10);

    rng('default')
    N = numel(leafs);
    err = zeros(N,1);
    for n=1:N
        t = fitctree(X,Y,'CrossVal','On',...
            'MinLeaf',leafs(n));
        err(n) = kfoldLoss(t);
    end
    if showGraphs == true
        plot(leafs,err);
        xlabel('Min Leaf Size');
        ylabel('cross-validated error');
    end
    [a,index] = min(err);

    %% 
    cols={'wAge', 'wEd', 'hEd', 'nChild', 'wRel','wWork', 'hOcc', 'stLiv','mExp'};
    %cols={'wEd', 'hEd', 'nChild', 'wRel','wWork', 'hOcc', 'stLiv','mExp'};
    otree = fitctree(X,Y,'PredictorNames',cols);
    %view(ctree,'mode','graph');

    Yfit = predict(otree,Xv);
    overfittedEff = sum(Yfit == Yv)/length(Yv);


    %% Predicts and evaluate results

    % Selecting the best level to prune
    [~,~,~,bestlevel] = cvLoss(otree,...
    'SubTrees','All','TreeSize','min');
    ptree = prune(otree,'Level',bestlevel);
    %view(ctree,'mode','graph');

    % New prediction
    Yfit = predict(ptree,Xv);
    pruneEff = sum(Yfit == Yv)/length(Yv);

    %Command used in matlabR2014, it might not work on more recent versions
    %for which might work the following line instead
    %ctree = fitctree(X,Y,'PredictorNames',cols,'MinLeafSize',round(leafs(index)));
    ctree = fitctree(X,Y,'PredictorNames',cols,'MinLeaf',round(leafs(index)));
    
    if showGraphs == true
        view(otree,'mode','graph');
        view(ptree,'mode','graph');
        view(ctree,'mode','graph');
    end

    Yfit = predict(ctree,Xv);
    MinLeafEff = sum(Yfit == Yv)/length(Yv);

    totals(1)=totals(1)+overfittedEff;
    totals(2)=totals(2)+pruneEff;
    totals(3)=totals(3)+MinLeafEff;

end

totals = totals / nTest;

fprintf(' Overfitted tree precision: %4.2f \n Pruned tree precision: %4.2f \n Min observation tree precision: %4.2f \n',totals(1)*100,totals(2)*100,totals(3)*100);